import XCTest

import libnetwork_iosTests

var tests = [XCTestCaseEntry]()
tests += libnetwork_iosTests.allTests()
XCTMain(tests)
