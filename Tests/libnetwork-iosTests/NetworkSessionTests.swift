import XCTest
import PromiseKit
@testable import libnetwork_ios

final class NetworkSessionTests: XCTestCase {
    var sessionMock: NetworkSessionMock!
    override func setUp() {
        sessionMock = NetworkSessionMock()
    }


    func testRetryFail() {
        enum Test: Error {
            case error
        }

        let expect = expectation(description: "Wait for retry")
        retry(times: 4, cooldown: 0) { () -> Promise<Void> in
            return Promise(error: Test.error)
        }.catch { error in
            expect.fulfill()
        }
        waitForExpectations(timeout: 1)
    }

    func testRetrySucceed() {
        enum Test: Error {
            case error
        }

        let expect = expectation(description: "Wait for retry")
        var count = 0
        retry(times: 4, cooldown: 0) { () -> Promise<Void> in
            count += 1
            if count == 5 {
                return Promise()
            }
            return Promise(error: Test.error)
        }
        .done { expect.fulfill() }
        .catch { error in
            XCTFail()
            expect.fulfill()
        }
        waitForExpectations(timeout: 1)
    }
    static var allTests = [
        ("testRetryFail", testRetryFail),
        ("testRetrySucceed", testRetrySucceed)
    ]
}
