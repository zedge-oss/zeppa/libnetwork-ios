import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(libnetwork_iosTests.allTests),
    ]
}
#endif
