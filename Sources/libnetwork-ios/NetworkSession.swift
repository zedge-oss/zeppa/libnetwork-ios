import Foundation
import PromiseKit
import PMKFoundation

public protocol NetworkSession {
    func loadData(with request: URLRequest) -> Promise<(data: Data, response: URLResponse)>
}

extension URLSession: NetworkSession {
    public func loadData(with request: URLRequest) -> Promise<(data: Data, response: URLResponse)> {
        return dataTask(.promise, with: request)
    }
}

public func retry<T>(times: Int, cooldown: TimeInterval, body: @escaping () -> Promise<T>) -> Promise<T> {
    var retryCounter = 0
    func attempt() -> Promise<T> {
        return body().recover(policy: CatchPolicy.allErrorsExceptCancellation) { error -> Promise<T> in
            retryCounter += 1
            guard retryCounter <= times else {
                throw error
            }
            return after(seconds: cooldown).then(attempt)
        }
    }
    return attempt()
}
