import Foundation
import PromiseKit
import PMKFoundation

@testable import libnetwork_ios

internal class NetworkSessionMock: NetworkSession {
    var data: Data?
    var request: URLRequest?
    var response: URLResponse?
    var error: Error?

    func loadData(with request: URLRequest) -> Promise<(data: Data, response: URLResponse)> {
        self.request = request
        return Promise<(data: Data, response: URLResponse)> { seal in
            switch (data, response, error) {
            case let (_, _, error?):
                seal.reject(error)
            case let (data?, response?, _):
                seal.fulfill((data, response))
            default:
                seal.reject(PMKError.invalidCallingConvention)
            }
        }
    }
}
